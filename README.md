# Container debug image

Just a container image that I plug volumes into to debug them. It contains the
following sofware:

* neovim
* bat
* ss
* curl
* exa
* dust
* ripgrep
* [fd](https://github.com/sharkdp/fd)
